import { Component } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { fromEvent, Observable } from 'rxjs';
import {first, take, tap} from 'rxjs/operators';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  res: any;
  mouse$: Observable<any>;

  clickedItem: any;

  constructor(private http: HttpClient) {
    this.getList('https://pokeapi.co/api/v2/pokemon');
  }

  getList(url: string){
    this.res = null;
    this.clickedItem = null;
    this.http.get<any>(url).subscribe(res =>{
      console.log(res);
      this.res = res;
      if(this.res.results != null && this.res.results.length > 0){
        this.clicked(this.res.results[0]);
      }
    });
  }


  async clicked(poke){
    this.clickedItem = null;
    this.clickedItem = await this.http.get(poke.url).toPromise();
    // console.log(this.clickedItem);
  }

  next(){
    let index = this.res.results.findIndex(x => x.name === this.clickedItem.name);
    if(index < this.res.results.length - 1){
      this.clicked(this.res.results[index + 1]);
    }
    else{
      this.clicked(this.res.results[0]);
    }
    
  }

  prev(){
    let index = this.res.results.findIndex(x => x.name === this.clickedItem.name);
    if(index > 0){
      this.clicked(this.res.results[index - 1]);
    }
    else{
      this.clicked(this.res.results[this.res.results.length - 1]);
    }
  }

}
